import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import "./assets/styles/tailwindcss.css";
import VueHotjar from "vue-hotjar-next";

createApp(App)
  .use(VueHotjar, {
    id: 2927810,
    isProduction: true,
    snippetVersion: 6,
  })
  .use(store)
  .use(router)
  .mount("#app");
