import { createRouter, createWebHistory } from "vue-router";

const routes = [
  {
    path: "/auto",
    name: "autodl",
    component: () => import("../views/tools/auto-dl.vue"),
  },
  {
    path: "/ner",
    name: "ner",
    component: () => import("../views/tools/ner.vue"),
  },
  {
    path: "/login",
    name: "login",
    component: () => import("../views/auth/login.vue"),
  },
  {
    path: "/",
    name: "home",
    component: () => import("../views/home.vue"),
  },
  {
    path: "/about",
    name: "about",
    component: () => import("../views/about.vue"),
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
