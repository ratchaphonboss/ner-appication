import Analytics from "analytics";
import segmentPlugin from "@analytics/segment";

const analytics = Analytics({
  app: "demo.rushai.dev",
  plugins: [
    segmentPlugin({
      writeKey: "HiMQQpdKmAX7uDUecDuRvy4skfwpnP2N",
    }),
  ],
});

export default {
  segment_identify(firstName, lastName) {
    analytics.identify("customer", {
      firstName: firstName,
      lastName: lastName,
    });
  },
  segment_run_experiment(dataset, algorithm) {
    analytics.track("run experiment", {
      dataset: dataset,
      algorithm: algorithm,
    });
  },
  segment_event(event, buttom) {
    analytics.track(event, {
      buttom: buttom,
    });
  },
};
