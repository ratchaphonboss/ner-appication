import axios from "axios";

const apiClient = axios.create({
  baseURL: "https://api.rushai.dev",
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json",
  },
});

export default {
  run_experiment(payload) {
    return apiClient.post("/pipeline", payload);
  },
};
