## ROUTERs
|  NAME | Url | Description |
|---|---|---|
| AutoDL |  /auto | This one will help with making POC of data obtained from the data warehouse and wanting to test whether the model structure is suitable, Data scientists will use it to build new model structures. |
| NER |  /ner | coming soon ... |


### ตัวอย่างการ run ด้วย Terraform
```javascript
resource "null_resource" "mac" {
  connection {
    type     = "ssh"
    user     = "xxxxxx"
    password = "xxxxxx"
    host     = "xxxxxx"
  }


  provisioner "remote-exec" {
    inline = [
      "echo 'Hello'"
    ]
  }

  provisioner "remote-exec" {
    inline = [
      "echo 'xxxxxx' | sudo -S -l",
      "ARGO_HOST=$(sudo k3s kubectl -n argocd get service argocd-server -o jsonpath='{.spec.clusterIP}')",
      "ARGO_PASS=$(sudo k3s kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath='{.data.password}' | base64 -d)",
      "echo 'y' | sudo -S ./argocd login $ARGO_HOST --username admin --password $ARGO_PASS",
      "sudo ./argocd repo add git@gitlab.com:ratchaphonboss/ner-appication.git --ssh-private-key-path ~/.ssh/id_rsa"
    ]
  }

  provisioner "remote-exec" {
    inline = [
      "echo 'xxxxxx' | sudo -S -l",
      "ARGO_HOST=$(sudo k3s kubectl -n argocd get service argocd-server -o jsonpath='{.spec.clusterIP}')",
      "ARGO_PASS=$(sudo k3s kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath='{.data.password}' | base64 -d)",
      "echo 'y' | sudo -S ./argocd login $ARGO_HOST --username admin --password $ARGO_PASS",
      "sudo k3s kubectl create namespace demo",
      "sudo ./argocd app create ner-appication --repo git@gitlab.com:ratchaphonboss/ner-appication.git --path deployment/dev --dest-server https://kubernetes.default.svc --dest-namespace default",
    ]
  }

  provisioner "remote-exec" {
    inline = [
      "echo 'xxxxxx' | sudo -S -l",
      "ARGO_HOST=$(sudo k3s kubectl -n argocd get service argocd-server -o jsonpath='{.spec.clusterIP}')",
      "ARGO_PASS=$(sudo k3s kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath='{.data.password}' | base64 -d)",
      "echo 'y' | sudo -S ./argocd login $ARGO_HOST --username admin --password $ARGO_PASS",
      "sudo ./argocd app set ner-appication --sync-policy automated",
    ]
  }
}
